<?php
/**
 * Created by PhpStorm.
 * User: aigie
 * Date: 19/11/2016
 * Time: 14:22
 */

use Catouyou\DAO\AdvertDAO;
use Catouyou\DAO\UserDAO;
use Symfony\Component\Debug\ErrorHandler;
use Symfony\Component\Debug\ExceptionHandler;
use Symfony\Component\HttpFoundation\Request;

ErrorHandler::register();
ExceptionHandler::register();

$app->register(new Silex\Provider\DoctrineServiceProvider());
$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__.'/../views',
));
$app->register(new Silex\Provider\RoutingServiceProvider());
$app->register(new Silex\Provider\SessionServiceProvider());
$app->register(new Silex\Provider\SecurityServiceProvider(), array(
    'security.firewalls' => array(
        'secured' => array(
            'pattern' => '^/',
            'anonymous' => true,
            'logout' => true,
            'form' => array('login_path' => '/login', 'check_path' => '/login_check'),
            'users' => function () use ($app) {
                return new UserDAO($app['db']);
            },
        ),
    ),
));


$app['dao.user'] = function ($app) {
    return new UserDAO($app['db']);
};
$app['dao.advert'] = function($app) {
    $advertDAO = new AdvertDAO($app['db']);
    $advertDAO->setUserDAO($app['dao.user']);
    return $advertDAO;
};

// Register JSON data decoder for JSON requests
$app->before(function (Request $request) {
    if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
        $data = json_decode($request->getContent(), true);
        $request->request->replace(is_array($data) ? $data : array());
    }
});
