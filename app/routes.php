<?php
/**
 * Created by PhpStorm.
 * User: aigie
 * Date: 19/11/2016
 * Time: 14:27
 */

use Catouyou\Catouyou\Advert;
use Symfony\Component\HttpFoundation\Request;
use Catouyou\Catouyou\User;

//Main view on web
$app->get('/', function() use($app) {
    return $app['twig']->render('index.html.twig');
});

// API

// USERS
// Get user
$app->get('/api/user/{id}', function ($id) use ($app) {

    $user = $app['dao.user']->find($id);
    $responseData = array(
        'id' => $user->getId(),
        'username' => $user->getUsername(),
        'email' => $user->getEmail(),
        'role' => $user->getRole()
    );

    return $app->json($responseData);
})->bind('api_user');

// Create user
$app->post('/api/user/create', function (Request $request) use ($app) {

    if (!$request->request->has('firstname')) {
        return $app->json('Missing parameter: firstname', 400);
    }
    if (!$request->request->has('lastname')) {
        return $app->json('Missing parameter: lastname', 400);
    }

    $user = new User();
    $user->setUsername($request->request->get('username'));
    $app['dao.user']->save($user);

    $responseData = array(
        'id' => $user->getId(),
        'username' => $user->getUsername(),
    );

    return $app->json($responseData, 201);
})->bind('api_user_add');

// Delete user
$app->delete('/api/user/delete/{id}', function ($id) use ($app) {
    $app['dao.user']->delete($id);

    return $app->json('No content', 204);
})->bind('api_user_delete');

// Update user
$app->put('/api/user/update/{id}', function ($id, Request $request) use ($app) {
    $user = $app['dao.user']->find($id);

    $user->setFirstname($request->request->get('firstname'));
    $user->setlastname($request->request->get('lastname'));
    $app['dao.user']->save($user);

    $responseData = array(
        'id' => $user->getId(),
        'firstname' => $user->getFirstname(),
        'lastname' => $user->getLastname()
    );

    return $app->json($responseData, 202);
})->bind('api_user_update');

// ADVERTS
// Add advert
$app->post('/api/advert/add', function(Request $request) use($app) {
    //On vérifie que tous les champs obligatoires sont fournis
    if (!$request->request->has('createdAt')) {
        return $app->json('Paramètre manquant : date', 400);
    }
    if (!$request->request->has('lat')) {
        return $app->json('Paramètre manquant : latitude', 400);
    }
    if (!$request->request->has('lng')) {
        return $app->json('Paramètre manquant : longitude', 400);
    }
    if (!$request->request->has('description')) {
        return $app->json('Paramètre manquant : description', 400);
    }
    if (!$request->request->has('birthDate')) {
        return $app->json('Paramètre manquant : date de naissance', 400);
    }
    if (!$request->request->has('sex')) {
        return $app->json('Paramètre manquant : sexe', 400);
    }
    if (!$request->request->has('image')) {
        return $app->json('Paramètre manquant : image', 400);
    }

    $data = $request->request;

    //On prépare et sauvegarde la nouvelle annonce
    $newAdvert = new Advert();
    $newAdvert->setCreatedAt($data->get('createdAt'));
    $newAdvert->setDescription($data->get('description'));
    $newAdvert->setBirthDate($data->get('birthDate'));
    $newAdvert->setLat($data->get('lat'));
    $newAdvert->setLng($data->get('lng'));
    $newAdvert->setImgURI($data->get('image'));
    $newAdvert->setSex($data->get('sex'));
    $newAdvert->setTattooed($data->get('tattooed'));
    $newAdvert->setVaccinated($data->get('vaccinated'));
    $newAdvert->setOwner($app['dao.user']->find($data->get('user_id')));

    $app['dao.advert']->save($newAdvert);

    $responseData = array(
        'id' => $app['dao.advert']->getLastInsertId(),
        'createdAt' => $newAdvert->getCreatedAt(),
        'birthDate' => $newAdvert->getBirthDate(),
        'description' => $newAdvert->getDescription(),
        'lat' => $newAdvert->getLat(),
        'lng' => $newAdvert->getLng(),
        'image' => $newAdvert->getImgURI(),
        'sex' => $newAdvert->getSex(),
        'tattooed' => $newAdvert->isTattooed(),
        'vaccinated' => $newAdvert->isVaccinated()
    );

    //On renvoie les infos de l'annonce
    return $app->json($responseData, 201);
})->bind('api_advert_add');

// Delete advert
$app->delete('/api/advert/delete/{id}', function($id) use($app) {
    $app['dao.advert']->delete($id);

    return $app->json('No content', 204);
})->bind('api_advert_delete');

//Get advert
$app->get('/api/advert/{id}', function($id) use($app) {
    $advert = $app['dao.advert']->find($id);

    $responseData = array(
        'id' => $advert->getId(),
        'createdAt' => $advert->getCreatedAt(),
        'birthDate' => $advert->getBirthDate(),
        'description' => $advert->getDescription(),
        'lat' => $advert->getLat(),
        'lng' => $advert->getLng(),
        'image' => $advert->getImgURI(),
        'sex' => $advert->getSex(),
        'tattooed' => $advert->isTattooed(),
        'vaccinated' => $advert->isVaccinated(),
        'owner' => [
            'username' => $advert->getOwner()->getUsername(),
        ]
    );

    return $app->json($responseData);
})->bind('api_advert');

//Get adverts
$app->get('/api/adverts', function() use ($app) {
    $adverts = $app['dao.advert']->findAll();
    $responseData = array();
    foreach ($adverts as $advert) {
        $responseData[] = array(
            'id' => $advert->getId(),
            'createdAt' => $advert->getCreatedAt(),
            'description' => $advert->getDescription(),
            'birthDate' => $advert->getBirthDate(),
            'lat' => $advert->getLat(),
            'lng' => $advert->getLng(),
            'image' => $advert->getImgURI(),
            'sex' => $advert->getSex(),
            'tattooed' => $advert->isTattooed(),
            'vaccinated' => $advert->isVaccinated(),
            'owner' => [
                'username' => $advert->getOwner()->getUsername(),
            ]
        );
    }
    return $app->json($responseData);
})->bind('api_adverts');

//Get relevant adverts
$app->get('/api/relevant', function(Request $request) use($app) {
    $lat = $request->query->has('lat')? $request->query->get('lat') : null;
    $lng = $request->query->has('lng')? $request->query->get('lng') : null;
    $distance = $request->query->has('distance')? $request->query->get('distance') : null;
    $age = $request->query->has('age')? $request->query->get('age') : null;
    $sex = $request->query->has('sex')? $request->query->get('sex') : null;
    $vaccinated = $request->query->has('vaccinated')? $request->query->get('vaccinated') : null;
    $tattooed = $request->query->has('tattooed')? $request->query->get('tattooed') : null;

    $adverts = $app['dao.advert']->findRelevantAdverts($lat, $lng, $distance, $age, $sex, $vaccinated, $tattooed);

    $responseData = array();
    foreach ($adverts as $advert) {
        $responseData[] = array(
            'id' => $advert->getId(),
            'createdAt' => $advert->getCreatedAt(),
            'birthDate' => $advert->getBirthDate(),
            'description' => $advert->getDescription(),
            'lat' => $advert->getLat(),
            'lng' => $advert->getLng(),
            'image' => $advert->getImgURI(),
            'sex' => $advert->getSex(),
            'tattooed' => $advert->isTattooed(),
            'vaccinated' => $advert->isVaccinated(),
            'owner' => [
                'username' => $advert->getOwner()->getUsername(),
            ]
        );
    }

    return $app->json($responseData);
})->bind('api_relevant');

// Get favorites adverts
$app->get('/api/favorites/{id}', function($id) use($app) {
    $adverts = $app['dao.advert']->findFavoritesByUser($id);
    $responseData = array();
    foreach ($adverts as $advert) {
        $responseData[] = array(
            'id' => $advert->getId(),
            'createdAt' => $advert->getCreatedAt(),
            'description' => $advert->getDescription(),
            'birthDate' => $advert->getBirthDate(),
            'lat' => $advert->getLat(),
            'lng' => $advert->getLng(),
            'image' => $advert->getImgURI(),
            'sex' => $advert->getSex(),
            'tattooed' => $advert->isTattooed(),
            'vaccinated' => $advert->isVaccinated(),
            'owner' => [
                'username' => $advert->getOwner()->getUsername(),
            ]
        );
    }

    return $app->json($responseData);
})->bind('api_favorites_adverts');