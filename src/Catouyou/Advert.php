<?php
/**
 * Created by PhpStorm.
 * User: aigie
 * Date: 19/11/2016
 * Time: 15:18
 */

namespace Catouyou\Catouyou;


use Catouyou\Catouyou\User;

class Advert
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var \DateTime
     */
    private $birthDate;

    /**
     * @var string
     */
    private $imgURI;

    /**
     * @var string
     */
    private $sex;

    /**
     * @var string
     */
    private $description;

    /**
     * @var boolean
     */
    private $tattooed;

    /**
     * @var boolean
     */
    private $vaccinated;

    /**
     * @var float
     */
    private $lat;

    /**
     * @var float
     */
    private $lng;

    /**
     * Associated owner
     *
     * @var \Catouyou\Catouyou\User
     */
    private $owner;

    /**
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $date
     */
    public function setCreatedAt($date)
    {
        $this->createdAt = $date;
    }

    /**
     * @return string
     */
    public function getImgURI()
    {
        return $this->imgURI;
    }

    /**
     * @param string $imgURI
     */
    public function setImgURI($imgURI)
    {
        $this->imgURI = $imgURI;
    }

    /**
     * @return mixed
     */
    public function getSex()
    {
        return $this->sex;
    }

    /**
     * @param mixed $sex
     */
    public function setSex($sex)
    {
        $this->sex = $sex;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return boolean
     */
    public function isTattooed()
    {
        return $this->tattooed;
    }

    /**
     * @param boolean $tattooed
     */
    public function setTattooed($tattooed)
    {
        $this->tattooed = $tattooed;
    }

    /**
     * @return boolean
     */
    public function isVaccinated()
    {
        return $this->vaccinated;
    }

    /**
     * @param boolean $vaccinated
     */
    public function setVaccinated($vaccinated)
    {
        $this->vaccinated = $vaccinated;
    }

    public function getOwner()
    {
        return $this->owner;
    }

    public function setOwner(\Catouyou\Catouyou\User $user)
    {
        $this->owner = $user;
    }

    /**
     * @return float
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * @param float $lat
     */
    public function setLat($lat)
    {
        $this->lat = $lat;
    }

    /**
     * @return float
     */
    public function getLng()
    {
        return $this->lng;
    }

    /**
     * @param float $lng
     */
    public function setLng($lng)
    {
        $this->lat = $lng;
    }

    /**
     * @return \DateTime
     */
    public function getBirthDate()
    {
        return $this->birthDate;
    }

    /**
     * @param \DateTime $birthDate
     */
    public function setBirthDate($birthDate)
    {
        $this->birthDate = $birthDate;
    }
}
