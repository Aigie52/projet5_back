<?php
/**
 * Created by PhpStorm.
 * User: aigie
 * Date: 24/11/2016
 * Time: 10:20
 */

namespace Catouyou\DAO;


use Catouyou\Catouyou\Advert;

class AdvertDAO extends DAO
{
    /**
     * @var \Catouyou\DAO\UserDAO
     */
    private $userDAO;

    public function setUserDAO(UserDAO $userDAO)
    {
        $this->userDAO = $userDAO;
    }

    public function find($id)
    {
        $sql = "SELECT * FROM advert WHERE id=?";
        $row = $this->getDb()->fetchAssoc($sql, array($id));

        if ($row) {
            return $this->buildDomainObject($row);
        } else {
            throw new \Exception("No advert matching id ".$id);
        }
    }

    public function findAll()
    {
        $sql = "SELECT * FROM advert";
        $row = $this->getDb()->fetchAll($sql);

        if ($row) {
            $adverts = array();
            foreach ($row as $advert){
                array_push($adverts, $this->buildDomainObject($advert));
            }
            return $adverts;
        } else {
            throw new \Exception("No advert");
        }
    }

    public function findRelevantAdverts($lat, $lng, $distance, $age, $sex, $vaccinated, $tattooed) {
        if($lat !== null && $lng !== null && $distance !== null) {
            $sql = "SELECT *, (
                    6371 * acos (
                        cos ( radians(" . $lat . ") )
                        * cos( radians( lat ) )
                        * cos( radians( lng ) - radians(" . $lng . ") )
                        + sin ( radians(" . $lat . ") )
                        * sin( radians( lat ) )
                    )
                ) AS distance
                FROM advert
                HAVING distance <= " . $distance . "";
        } else {
            $sql = "SELECT * FROM advert WHERE id > 0";
        }
        if($sex !== null) $sql .= " AND sex = '" . $sex ."'";
        if($vaccinated !== null) $sql .= " AND vaccinated = " . $vaccinated;
        if($tattooed !== null) $sql .= " AND tattooed = " . $tattooed;

        if($age !== null) {
            $sql .= " AND DATEDIFF(CURDATE(), birthDate) <= " . (30*$age) . "";
        }

        $data = $this->getDb()->fetchAll($sql);

        if ($data) {
            $adverts = array();
            foreach ($data as $advert){
                array_push($adverts, $this->buildDomainObject($advert));
            }
            return $adverts;
        } else {
            throw new \Exception("No relevant advert");
        }
    }

    public function findFavoritesByUser($userId)
    {
        $sql = "SELECT * FROM advert_user WHERE user_id=?";
        $data = $this->getDb()->fetchAll($sql, array($userId));

        if ($data) {
            $adverts = array();
            foreach ($data as $child){
                $id = $child['advert_id'];
                $advert = $this->find($id);
                array_push($adverts, $advert);
            }
            return $adverts;
        } else {
            throw new \Exception("No favorite adverts for user id ".$userId);
        }
    }

    public function save(Advert $advert)
    {
        $advertData = array(
            'createdAt' => $advert->getCreatedAt(),
            'birthDate' => $advert->getBirthDate(),
            'description' => $advert->getDescription(),
            'lat' => $advert->getLat(),
            'lng' => $advert->getLng(),
            'sex' => $advert->getSex(),
            'tattooed' => $advert->isTattooed(),
            'vaccinated' => $advert->isVaccinated(),
            'imgURI' => $advert->getImgURI(),
            'user_id' => $advert->getOwner()->getId()
        );

        // TODO CHECK
        if ($advert->getId()) {
            $this->getDb()->update('advert', $advertData, array('id' => $advert->getId()));
        } else {
            $this->getDb()->insert('advert', $advertData);
        }
    }

    public function delete($id)
    {
        $this->getDb()->delete('advert', array('id' => $id));
    }

    public function getLastInsertId()
    {
        return $this->getDb()->lastInsertId();
    }

    public function buildDomainObject($row)
    {
        $advert = new Advert();
        $advert->setId($row['id']);
        $advert->setCreatedAt($row['createdAt']);
        $advert->setDescription($row['description']);
        $advert->setLat($row['lat']);
        $advert->setLng($row['lng']);
        $advert->setSex($row['sex']);
        $advert->setTattooed($row['tattooed']);
        $advert->setVaccinated($row['vaccinated']);
        $advert->setImgURI($row['imgURI']);

        if(array_key_exists('user_id', $row)) {
            $userId = $row['user_id'];
            $user = $this->userDAO->find($userId);

            $advert->setOwner($user);
        }

        return $advert;
    }
}
