<?php
/**
 * Created by PhpStorm.
 * User: aigie
 * Date: 24/11/2016
 * Time: 10:23
 */

namespace Catouyou\DAO;

use Doctrine\DBAL\Connection;

abstract class DAO
{
    /**
     * Database connection
     *
     * @var \Doctrine\DBAL\Connection
     */
    private $db;

    /**
     * DAO constructor.
     * @param Connection $db
     */
    public function __construct(Connection $db)
    {
        $this->db = $db;
    }

    /**
     * Grants access to the database connection object
     * @return Connection
     */
    protected function getDb()
    {
        return $this->db;
    }

    /**
     * Builds a domain object from a DB row
     * Must be overridden by child classes
     */
    protected abstract function buildDomainObject($row);
}