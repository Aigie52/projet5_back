<?php
/**
 * Created by PhpStorm.
 * User: aigie
 * Date: 19/11/2016
 * Time: 14:26
 */

namespace Catouyou\DAO;

use Catouyou\Catouyou\User;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class UserDAO extends DAO implements UserProviderInterface
{
    public function find($id)
    {
        $sql = "SELECT * FROM user WHERE id=?";
        $row = $this->getDb()->fetchAssoc($sql, array($id));

        if ($row) {
            return $this->buildDomainObject($row);
        } else {
            throw new \Exception("No user matching id ".$id);
        }
    }

    public function loadUserByUsername($username)
    {
        $sql = "SELECT * FROM user WHERE username=?";
        $row = $this->getDb()->fetchAssoc($sql, array($username));

        if ($row) {
            return $this->buildDomainObject($row);
        } else {
            throw new UsernameNotFoundException(sprintf('User "%s" not found.', $username));
        }
    }

    public function refreshUser(UserInterface $user)
    {
        $class = get_class($user);
        if (!$this->supportsClass($class)) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', $class));
        }
        return $this->loadUserByUsername($user->getUsername());
    }

    public function supportsClass($class)
    {
        return 'Catouyou\User' === $class;
    }

    public function save(User $user)
    {
        $userData = array(
            'username' => $user->getUsername()
        );

        // TODO CHECK
        if ($user->getId()) {
            $this->getDb()->update('user', $userData, array('id' => $user->getId()));
        } else {
            $this->getDb()->insert('user', $userData);
        }
    }

    public function delete($id)
    {
        $this->getDb()->delete('user', array('id' => $id));
    }

    /**
     * Creates an User object based on a DB row
     * @param array $row containing User data
     * @return User
     */
    protected function buildDomainObject($row)
    {
        $user = new User();
        $user->setId($row['id']);
        $user->setUsername($row['username']);
        $user->setPassword($row['password']);
        $user->setSalt($row['salt']);
        $user->setRole($row['role']);

        return $user;
    }
}