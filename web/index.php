<?php
/**
 * Created by PhpStorm.
 * User: aigie
 * Date: 18/11/2016
 * Time: 14:23
 */

require_once __DIR__.'/../vendor/autoload.php';

$app = new Silex\Application();

require __DIR__.'/../app/config/dev.php';
require __DIR__.'/../app/app.php';
require __DIR__.'/../app/routes.php';

$app->run();
